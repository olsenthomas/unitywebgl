# http://rubylogs.com/writing-rails-middleware/

class UnityWebgl


  def initialize(app)
    @app = app
  end

  def call(env)
    path   = env['PATH_INFO'].chomp('/')    
    ext    = ::File.extname(path)
    method = env['REQUEST_METHOD']

    analytics =  env['HTTP_UNITYWEBGL']
    requestedHeaders = env['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']

    # The pre flight CORS request does not allow custom headers and we therefore
    # have to check the request header to check for our custom header  
    if (not requestedHeaders.nil?) and requestedHeaders.downcase.include? "unitywebgl"
      analytics = "yes"  
    end

    # http://www.html5rocks.com/en/tutorials/cors/
    if not analytics.blank?
      h = Hash.new
      h['Access-Control-Allow-Origin'] = "*"

      # Test if it is the pre flight CORS request
      if method == "OPTIONS"
        
        h['Access-Control-Allow-Methods'] = "POST"
        h['Access-Control-Max-Age'] = "300"
        h['Access-Control-Allow-Headers'] = requestedHeaders unless requestedHeaders.blank?           

        # return a pre flight response without further processing 
        return [200, h, ["hest pest hest"]]
      end
    end
    

    # This is a hack if you run the app on facebook
    # Facebook will use http method POST (such that it is possible pass on some values if specified) 
    # but we will change it to GET since we dont have a route for POST
    if path.downcase.include? "/webgl/index.html"
      env['REQUEST_METHOD'] = "GET"
    end

    @status, @headers, @response = @app.call(env)    

    #Rails.logger.debug "UnityWebgl: path = " +  path + " method = " + method + " ext = " + ext
    #Rails.logger.debug env.inspect

    if ext == ".jsgz"
      @headers['Content-Encoding'] = "gzip"
      @headers['Content-Type'] = "text/javascript"
    end

    if ext == ".datagz"
      @headers['Content-Encoding'] = "gzip"
      @headers['Content-Type'] = "text/plain"
    end

    if ext == ".memgz"
      @headers['Content-Encoding'] = "gzip"
      @headers['Content-Type'] = "text/plain"
    end
 

    [@status, @headers, @response]
  end

end
