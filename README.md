# README #


### What is this repository for? ###

This Rails middleware will enable a Rails site to serve the compressed version of a Unity WebGL game instead of the non compressed version which takes up a lot more space. This is useful if you want to keep the code on your heroku app instead of some third party storage.

In addition it also enable the possibility for the Unity WebGL Game to upload stats to your Rails. This is not working out of the box in the WebGL version because Unince has implemented the CORS handshake.

I am sure this could have been put together via some other plugin and configuration. But I thought it was fun to try and write some middleware. 

But dont hesitate to let me know about some easier way anyway.

### How do I get set up? ###

* Place unity_webgl.rb in your rails lib folder
* Add the below line to config/application.rb file
  config.middleware.insert_before "ActionDispatch::Static", "UnityWebgl" 

### Who do I talk to? ###

* Tweet @meat_and_fruit if you want to get in contact with me